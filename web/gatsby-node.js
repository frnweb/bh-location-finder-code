// web/gatsby-node.js

// ...


async function createLocationPages (graphql, actions) {
  // Get Gatsby‘s method for creating new pages
  const {createPage} = actions
  // Query Gatsby‘s GraphAPI for all the categories that come from Sanity
  // You can query this API on http://localhost:8000/___graphql
  const result = await graphql(`{
    allSanityLocation(filter: {services: {elemMatch: {serviceLine: {name: {eq: "Psychiatry"}}}}}) {
      nodes {
        id
        slug {
          current
        }
      }
    }
  }
  `)

  // If there are any errors in the query, cancel the build and tell us
  if (result.errors) throw result.errors

  // Let‘s gracefully handle if allSanityLocation is null
  const locationNodes = (result.data.allSanityLocation || {}).nodes || []

  locationNodes
    // Loop through the location nodes, but don't return anything
    .forEach((node) => {
      // Destructure the id and slug fields for each category
      const {id, slug = {}} = node
      // If there isn't a slug, we want to do nothing
      if (!slug) return

      // Make the URL with the current slug
      const path = `/${slug.current}`

      // Create the page using the URL path and the template file, and pass down the id
      // that we can use to query for the right category in the template file
      createPage({
        path,
        component: require.resolve('./src/templates/location.js'),
        context: {id}
      })
    })
}

exports.createPages = async ({graphql, actions}) => {
  await createLocationPages(graphql, actions) // <= add the function here
}