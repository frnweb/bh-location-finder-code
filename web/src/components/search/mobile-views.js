import React, {ReactDOM, useState } from 'react'
import { Modal, Nav, Tab } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { InstantSearch } from 'react-instantsearch-dom';

import FilterSideBar from "./filters/filtersidebar"
import Results from "./results"
import GoogleMaps from "./googlemap/googlemaps"

const MobileViews = ({
  searchState,
  searchClient,
  onSearchStateChange,
}) => {

  const [show, setShow] = useState(false);
  
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  function filterActive() {
    if (searchState.refinementList !== undefined) {
    let found = Object.entries(searchState.refinementList).some(
        (entry) => { 
          const [key, value] = entry; 
          return (value instanceof Array);
        });
    return found
    }
  }

  return (
    <> 
      <Tab.Container className="hide-for-large" defaultActiveKey="list-view">

        {/* Toolbar */}
        <div className="sl_toolbar hide-for-large">
          <div className={filterActive() === true ? 'sl_toolbar__filter sl_active' : 'sl_toolbar__filter sl_inactive'} onClick={handleShow} onKeyDown={handleShow}>
            Filters
          </div>
          <Nav className="sl_toolbar__toggle" variant="pills">
            <Nav.Item className="sl_toolbar__toggles sl_toolbar__list">
              <Nav.Link eventKey="list-view">List</Nav.Link>
            </Nav.Item>
            <Nav.Item className="sl_toolbar__toggles sl_toolbar__map">
              <Nav.Link eventKey="map-view">Map</Nav.Link>
            </Nav.Item>
          </Nav>
        </div>

        <Tab.Content className="sl_container sl_container--search hide-for-large">

        {/* List View */}
        <Tab.Pane className="sl_results" eventKey="list-view">
          <Results />
        </Tab.Pane>

        {/* Map View */}
        <Tab.Pane eventKey="map-view" unmountOnExit={true}>
          <GoogleMaps />
        </Tab.Pane>
        
        </Tab.Content>

      </Tab.Container>

      {/* Filter Popup */}
      <Modal
      show={show}
      onHide={handleClose}
      size="xl"
      dialogClassName="sl_modal sl_modal--filter"
      contentClassName="sl_modal__content"
      centered 
      >
      <InstantSearch
        searchClient={searchClient}
        indexName="BH_Location"
        searchState={searchState}
        onSearchStateChange={onSearchStateChange}
      >
      <Modal.Body>
          <div className="sl_modal__close" onClick={ handleClose }>Back to Search</div>
          <div className="sl_filter">
            <FilterSideBar />
          </div>
      </Modal.Body>
      </InstantSearch>
      </Modal>
      {/* End Filter Popup */}
    </>

  )
};

MobileViews.propTypes = {
  searchState: PropTypes.object.isRequired,
  searchClient: PropTypes.object.isRequired,
  onSearchStateChange: PropTypes.func.isRequired,
};

export default MobileViews