import React from "react"

import {
  connectRange,
} from 'react-instantsearch-dom';

import 'rheostat/initialize';
import Rheostat from 'rheostat'
import 'rheostat/css/rheostat.css';

const RangeSliderMin = ({ min, max, currentRefinement, refine }) => {
  const [stateMax, setStateMax] = React.useState(max);

  const onChange = ({ values: [max] }) => {
    refine({ max });
  };

  const onValuesUpdated = ({ values: [max] }) => {
      setStateMax(max);
  };

  return (
    <Rheostat
      min={min}
      max={max}
      values={[
        max
      ]}
      onChange={onChange}
      onValuesUpdated={onValuesUpdated}
      
    >
      <div className="sl_filter__slider__value" style={{ left: 0 }}>{stateMax}</div>
    </Rheostat>
  );
};

const CustomRangeSliderMin = connectRange(RangeSliderMin)

export default CustomRangeSliderMin