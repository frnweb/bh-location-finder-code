import React from "react"

import {
  connectRange,
} from 'react-instantsearch-dom';

import 'rheostat/initialize';
import Rheostat from 'rheostat'
import 'rheostat/css/rheostat.css';

const RangeSliderMax = ({ min, max, currentRefinement, refine }) => {

  const [stateMin, setStateMin] = React.useState(min);

  const onChange = ({ values: [min] }) => {
      refine({ min });
    };

  const onValuesUpdated = ({ values: [min] }) => {
    setStateMin(min);
  };

  return (
    <Rheostat
      min={min}
      values={[
        min
      ]}
      onChange={onChange}
      onValuesUpdated={onValuesUpdated}
    >
      <div className="sl_filter__slider__value" style={{ right: 0 }}>{stateMin}</div>
    </Rheostat>
  );
};

const CustomRangeSliderMax = connectRange(RangeSliderMax)

export default CustomRangeSliderMax