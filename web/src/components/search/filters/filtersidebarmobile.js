import React, { useContext } from 'react';
import { MenuContext } from 'react-flexible-sliding-menu';
import { RefinementList } from 'react-instantsearch-dom';
import { orderBy } from 'lodash'

import CustomCurrentRefinements from "../currentrefinements"
import CustomRangeSliderMin from "./filtermin"
import CustomRangeSliderMax from "./filtermax"

const FilterSideBar = () => {
    const { toggleMenu } = useContext(MenuContext);

    return (
        <>
        <div className="sl_modal__close" onClick={toggleMenu}>Back to Search</div>
          <div className="sl_filter">
            <CustomCurrentRefinements />
            <div className="sl_filter__cat">
                <p className="sl_filter__title">Levels of Care</p>
                <RefinementList 
                    attribute="services.levelCare.name"
                    transformItems={items => orderBy(items, "label", "asc")}
                    showMore
                />
            </div>
            <div className="sl_filter__cat">
                <p className="sl_filter__title">Ages Treated</p>
                <div className="sl_filter__slider">
                    <CustomRangeSliderMin min={4} attribute="services.age.max"  />
                    <CustomRangeSliderMax max={100} attribute="services.age.min" />
                </div>
            </div>
            <div className="sl_filter__cat">
                <p className="sl_filter__title">Specialty Programs</p>
                <RefinementList 
                showMore
                attribute="specialty" 
                transformItems={items => orderBy(items, "label", "asc")}
                translations={{
                    showMore(expanded) {
                      return expanded ? 'See Less Categories' : 'See All Categories';
                    }
                }}
                />
            </div>
            <div className="sl_filter__callout">
                <p>Visit our website to see a map of all UHS locations including Acute Care hospitals, FEDs, Ambulatory Centers, and Physician Networks.</p>
                <a className="sl_button sl_button--border sl_button--external" href="https://www.uhsinc.com/our-communities/" target="_blank" rel="noreferrer">View All Locations</a>
            </div>
        </div>
        </>
    );    
};
export default FilterSideBar