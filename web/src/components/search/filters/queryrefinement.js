import React from "react"

import {
    connectCurrentRefinements
  } from 'react-instantsearch-dom';

  const QueryRefinement = ( {items, refine} ) => (
      //Check if any filters are applied
      items.length > 0 && items[0].id === "query" ? (
        <div className="sl_results__filter">
          <p>Showing results for: <span>"{items[0].currentRefinement}"</span></p>
        </div>
      )
    
      // If no filters do nothing
      : null

  );

const CustomQueryRefinement = connectCurrentRefinements(QueryRefinement);
export default CustomQueryRefinement;