/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

 import React, {Component} from "react"
 import propTypes from "prop-types"
 import {
   InstantSearch,
   Configure,
   connectRefinementList
 } from 'react-instantsearch-dom';
 import algoliasearch from 'algoliasearch';
 import qs from 'qs';
 
 import 'bootstrap/dist/css/bootstrap.min.css';
 import "../../styles/main.scss";
 
 // Include only the reset
 import 'instantsearch.css/themes/reset.css';

 const VirtualRefinementList = connectRefinementList(() => null);
 const DEBOUNCE_TIME = 200;
 const searchClient = algoliasearch(process.env.GATSBY_ALGOLIA_APP_ID, process.env.GATSBY_ALGOLIA_SEARCH_KEY);
 
 const createURL = state => `?${qs.stringify(state)}`;
 
 const searchStateToUrl = (props, searchState) =>
   searchState ? `/search/${createURL(searchState)}` : '';
 
 const urlToSearchState = location => qs.parse(location.search.slice(1));

class Wrapper extends Component { 
  render(){
    const { searchState } = this.state;
    return (
      <>
        <InstantSearch
          indexName="BH_Location" 
          searchClient={searchClient}
          searchState={searchState}
          onSearchStateChange={this.onSearchStateChange}
          createURL={createURL}
        >
        <Configure
          hitsPerPage={400}
        />
        <VirtualRefinementList attribute="services.levelCare.name" />
        <VirtualRefinementList attribute="specialty" />
        {React.cloneElement(this.props.children, {searchClient: searchClient, searchState: searchState, onSearchStateChange: this.onSearchStateChange})}
        </InstantSearch>
      </>
     )
   }
 }

Wrapper.propTypes = {
  children: propTypes.node.isRequired,
}

export default Wrapper;