import React from "react"

const Footer = () => (
    <footer style={{
        marginTop: `2rem`
      }}>
        This is where the Footer will go across the site
      </footer>
)

export default Footer